<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" >
	<title>First jsp called from controller</title>
</head>
<body>
	<h1 class="first">Je m'appelle ${prenom} ${nom}</h1>
</body>
</html>